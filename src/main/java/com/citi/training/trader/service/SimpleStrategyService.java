package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.SimpleStrategyDao;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;

@Component
public class SimpleStrategyService {

    @Autowired
    private SimpleStrategyDao simpleStrategyDao;

    public List<SimpleStrategy> findAll(){
        return simpleStrategyDao.findAll();
    }

    public int save(SimpleStrategy strategy) {
        return simpleStrategyDao.save(strategy);
    }
    
    //need to get last strategy last row possible
    public SimpleStrategy findById(int id) {
      return new SimpleStrategy(new Stock(id, "AAA "), 1000);
    }
}
