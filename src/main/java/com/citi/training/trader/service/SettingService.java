package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.mysql.MysqlSettingsDao;
import com.citi.training.trader.model.Setting;

@Component
public class SettingService {

	
	@Autowired
	private MysqlSettingsDao settingsdao;
	
	public List<Setting> getAll(){
		return settingsdao.getAllSettings();
	}

	public int save(Setting setting) {
		
		return settingsdao.saveSetting(setting);
	}

}
