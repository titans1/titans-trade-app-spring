package com.citi.training.trader.model;

public class Setting {

  private int id;
  private int status;
  
  
  
  public Setting(int id, int status){
  	this.setId(id);
  	this.setStatus(status);
  }
	public Setting() {
		// TODO Auto-generated constructor stub
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return  "Id [id=" + id + ", status=" + status + "]";
	}

}
