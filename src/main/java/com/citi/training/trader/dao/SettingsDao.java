package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.Setting;

public interface SettingsDao {
	Setting getSettingById(int id);
	int saveSetting(Setting setting);
	List<Setting> getAllSettings();
}
