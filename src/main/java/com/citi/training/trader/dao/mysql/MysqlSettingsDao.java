package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.SettingsDao;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.Setting;


@Component
public class MysqlSettingsDao implements SettingsDao {

	private static final Logger logger = LoggerFactory.getLogger(MysqlSimpleStrategyDao.class);

	private static String GETALL = "select setting.id as setting_id, setting.status as setting_status from setting ";

	private static String UPDATE = "UPDATE setting " + 
			"SET status =:status " + 
			"WHERE  id = 1 ";

	public MysqlSettingsDao() {

	}

	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public int saveSetting(Setting s) {
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("status",s.getStatus());
		KeyHolder keyHolder = new GeneratedKeyHolder();
		int rows = namedParameterJdbcTemplate.update(UPDATE, namedParameters, keyHolder);
		logger.debug("Logging update");
//		List <Setting> settings = tpl.query(UPDATE, new SettingMapper());
		return rows;
	}

	@Override
	public Setting getSettingById(int id) {


		return null;

	}

	@Override
	public List<Setting> getAllSettings() {
		return tpl.query(GETALL, new SettingMapper());
	
	}

	/**
	 * private internal class to map database rows to SimpleStrategy objects.
	 *
	 */
	private static final class SettingMapper implements RowMapper<Setting> {
		public Setting mapRow(ResultSet rs, int rowNum) throws SQLException {
			logger.debug("Mapping setting result set row num ");

			return new Setting(rs.getInt("setting_id"), rs.getInt("setting_status"));

		}
	}

}
