package com.citi.training.trader.strategy;

import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.List;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.mysql.MysqlSettingsDao;
//import com.citi.training.trader.dao.mysql.MysqlPriceDao;
import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Setting;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.SettingService;
import com.citi.training.trader.service.SimpleStrategyService;
import com.citi.training.trader.service.TradeService;

/**
 * A model object to hold the data for an instance of the MovingAveragesStrategy
 * algorithm.
 *
 * This strategy will trade for maxTrades and then exit.
 * 
 * See {@link com.citi.training.trader.strategy.SimpleStrategyAlgorithm}.
 *
 */
@Component
public class SimpleStrategyAlgorithm implements StrategyAlgorithm {

	private static final Logger logger = LoggerFactory.getLogger(SimpleStrategyAlgorithm.class);

	private double lastAveragePriceDifference = 0, exitProfit = 0, exitLoss = 0;

	private int shortHigher = 0;

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private PriceService priceService;

	@Autowired
	private SimpleStrategyService strategyService;

	@Autowired
	private TradeService tradeService;
	
	@Autowired 
	private SettingService settingService;


	

	@Value("${price_service_feed_poll_ms:15000}")
	private int pollfreq;

	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	public void run() {
		// 1 strategy at a time
		// use most recent strategy
		
		
		logger.info("Trading setting is "+ settingService.getAll().get(0).getStatus());
		int tradingStatus = settingService.getAll().get(0).getStatus();
		
		boolean doTrading = true;

		if(tradingStatus==0) {
			doTrading=false;
		}else {
			doTrading=true;
		}

		// get list of strategies
		List<SimpleStrategy> strategies = strategyService.findAll();

		// only use the most recent i.e (one strategy at a time)
		SimpleStrategy strategy = strategies.get(strategies.size() - 1);

		
		//if trading is true -> keeping trading with this strategy OR
		// if trading false and strategy still open i.e null or not a date -> run loop to close positions
		//loop is then broken out of  after these are closed

		if (doTrading && strategy.getStopped() == null || !doTrading && strategy.getStopped() == null) {
				
			Trade lastTrade = null;
			// get latest trade by current strategy id
			try {
				lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
				logger.info("Trade" + strategy.getId());
			} catch (TradeNotFoundException ex) {
				logger.debug("No Trades for strategy id: " + strategy.getId());
			}

			// wait for last trade if trade waiting for reply go to next iteration
			if (lastTrade != null) {
				
				if (lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
					logger.debug("Waiting for last trade to complete, do nothing");
					return;
					
				} else {
					accountTrade(lastTrade, strategy, doTrading);
				}
				
			}
			
			if(!doTrading) {
				logger.debug("Manually stopped, exiting strategy");
				strategy.setStopped(new Date());
				strategyService.save(strategy);
			}

			// declare variables to hold average price information
			double shortAverage = 0;
			double longAverage = 0;

			// averagePrice returns average price data for a given time period, based on RHS
			// side parameters
			shortAverage = averagePrice(strategy, 0.25);
			longAverage = averagePrice(strategy, 1);
			logger.info("the short average price for this time period is:" + shortAverage);
			logger.info("the long average price for this time period is:" + longAverage);

			// if either average is zero (i.e. there isn't enough information to determine
			// average), continue
			if (shortAverage == 0 | longAverage == 0) {
				logger.debug("Insufficient data for averages.... waiting....");
			
			}

			// calculates differences in two averages
			double currentAveragePriceDifference = (longAverage - shortAverage);
			logger.debug("currentPriceDifferenfce calculated as: "+currentAveragePriceDifference);

			double initalBuyPrice = 0;
			
			// if we have no strategy, take a strategy!!
			if (!strategy.hasPosition()) {

				// if the last average hasn't been set yet
				if (lastAveragePriceDifference == 0) {
					// logger.info("lastAveragePriceDifference: "+lastAveragePriceDifference);

					// set whether the price difference is higher or lower now
					if (currentAveragePriceDifference > 0) {
						// flag shows initial short ave was lower than long ave
						shortHigher = 1;
						logger.debug("Initial short average lower than long.");
						lastAveragePriceDifference = currentAveragePriceDifference;
						logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);
					} else if (currentAveragePriceDifference < 0) {
						// flag shows initial short ave was higher than long ave
						shortHigher = -1;
						logger.debug("Initial short average higher than long.");
						lastAveragePriceDifference = currentAveragePriceDifference;
						logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);
					} else {
						// if its exactly equal to 0 waits for next set of data to choose a strategy
						logger.debug("Initial short average and long average equal... wait to take position.");
//						lastAveragePriceDifference = currentAveragePriceDifference;
//
//						logger.debug("Last Price difference is still 0");

						
					}

					//lastAveragePriceDifference = currentAveragePriceDifference;
					//logger.debug("Last Price difference now: " + lastAveragePriceDifference);

				} else {

					// if the short ave was higher
//					if (shortHigher == 0) {
//
//						lastAveragePriceDifference = currentAveragePriceDifference;
//						logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);
//						
//					} else 
					if (shortHigher == 1) {
						
						logger.debug("Current Price difference: " + currentAveragePriceDifference);
						// check if the short ave has crossed over the long ave

						if (currentAveragePriceDifference < 0) {
							// if it has buy
							logger.debug("Short ave has crossed long BUYING.");
							initalBuyPrice = makeTrade(strategy, Trade.TradeType.BUY);
							// set initial price
							logger.debug("Bought at: " + initalBuyPrice);
							// TODO: Change this to the % value given by the user on the front end!!
							// or maybe not!!
							exitProfit = initalBuyPrice + (initalBuyPrice * 0.01);
							exitLoss = initalBuyPrice - (initalBuyPrice * 0.01);

						} else {
							// if not wait
							lastAveragePriceDifference = currentAveragePriceDifference;

							logger.debug("Last Price difference now: " + lastAveragePriceDifference);
							
						}
//						lastAveragePriceDifference = currentAveragePriceDifference;
//						logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);
//
//						lastAveragePriceDifference = currentAveragePriceDifference;
//						logger.debug("Last Price difference now: " + lastAveragePriceDifference);

						// or else if the short ave was lover
					} else if (shortHigher == -1) {
						logger.debug("Current Price difference: " + currentAveragePriceDifference);
						if (currentAveragePriceDifference > 0) {
							// if it has sell
							logger.debug("Short ave has crossed long SELLING.");
							initalBuyPrice = makeTrade(strategy, Trade.TradeType.SELL);
							// set initial price
							logger.debug("Bought at: " + initalBuyPrice);
							// TODO: Change this to the % value given by the user on the front end!!
							exitProfit = initalBuyPrice + (initalBuyPrice * 0.01);
							exitLoss = initalBuyPrice - (initalBuyPrice * 0.01);

						} else {
							// if not wait
							lastAveragePriceDifference = currentAveragePriceDifference;

							logger.debug("Last Price difference now: " + lastAveragePriceDifference);
					
						}
//
//						lastAveragePriceDifference = currentAveragePriceDifference;
//
//						logger.debug("Last Price difference now: " + lastAveragePriceDifference);
					}

				}

			} else if (strategy.hasLongPosition()) {
				// we have a long position => close the position by selling

				List<Price> lastPrice = priceService.findLatest(strategy.getStock(), 1);
				double currentPrice = lastPrice.get(0).getPrice();
				// calculate price diff % here if 0.01% P or L close position
				// else continue
				if (currentPrice > exitProfit || currentPrice < exitLoss) {
					logger.debug("Profit / Loss margin met, selling back stock.");
					makeTrade(strategy, Trade.TradeType.SELL);
					resetTradeVariables();
				} else {
					logger.debug("Insufficient price change between short and long averages, taking no action");
					lastAveragePriceDifference = currentAveragePriceDifference;
					logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);
					
				}

//				lastAveragePriceDifference = currentAveragePriceDifference;
//				logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);

			} else if (strategy.hasShortPosition()) {
				// we have a short position => close the position by buying
				
				List<Price> lastPrice = priceService.findLatest(strategy.getStock(), 1);
				double currentPrice = lastPrice.get(0).getPrice();
				// calculate price diff % here if 0.01% P or L close position
				// else continue
				if (currentPrice > exitProfit || currentPrice < exitLoss) {
					logger.debug("Profit / Loss margin met, buying back stock.");
					makeTrade(strategy, Trade.TradeType.BUY);
					resetTradeVariables();
				} else {
					logger.debug("Insufficient price change between short and long averages, taking no action");
					lastAveragePriceDifference = currentAveragePriceDifference;
					logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);
				}

//				lastAveragePriceDifference = currentAveragePriceDifference;
//				logger.debug("LastAveragePrice set to: " + lastAveragePriceDifference);

			}
			
			strategyService.save(strategy);

		}else {
    	logger.debug("Trading off");
		}

	}

		

	private double averagePrice(SimpleStrategy strategy, double minutes) {

		double timeInMs = (minutes * 60000);
		int timeAsInt = (int) timeInMs;

		Date PreviousDate = new Date(System.currentTimeMillis() - timeAsInt);
		Date currentDate = new Date(System.currentTimeMillis());

		// used to format the date's data previously obtained
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		// both dates formatted
		String currentDateFormatted = simpleDateFormat.format(currentDate);
		String previousDateFormatted = simpleDateFormat.format(PreviousDate);
		//logger.debug("The current date input here: " + currentDateFormatted);
		//logger.debug("The past date input here: " + previousDateFormatted);
		logger.debug("Ave Price for " + minutes + " minues is: "
				+ priceService.averagePrice(previousDateFormatted, currentDateFormatted));

		return priceService.averagePrice(previousDateFormatted, currentDateFormatted);

	}

	private void closePosition(double profitLoss, SimpleStrategy strategy) {
		logger.debug("Recording profit/loss of: " + profitLoss + " for strategy: " + strategy);
		strategy.addProfitLoss(profitLoss);
		strategy.closePosition();

		// TODO: set this to a different profit / loss?? do we want to make low return
		// trades till we've made a higher profit?
		if (strategy.getProfit() >= strategy.getExitProfitLoss()) {
			logger.debug("Exit condition reached, exiting strategy");
			strategy.setStopped(new Date());
			strategyService.save(strategy);
		}
	}

	private double makeTrade(SimpleStrategy strategy, Trade.TradeType tradeType) {
		Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
		tradeSender.sendTrade(new Trade(currentPrice.getPrice(), strategy.getSize(), tradeType, strategy));
		return currentPrice.getPrice();
	}
	
	private void accountTrade(Trade lastTrade, SimpleStrategy strategy, Boolean doTrading) {
		
		if (lastTrade.getAccountedFor()) {
			// return new Error
			return;
		}
			// account for last trade?
			if (lastTrade.getState() == TradeState.FILLED) {
				logger.debug("Accounting for Trade: " + lastTrade);
				
				if (!strategy.hasPosition()) {
					if (lastTrade.getTradeType() == Trade.TradeType.SELL) {
						logger.debug("Confirmed short position for strategy: " + strategy);
						strategy.takeShortPosition();
						if(!doTrading) {
							makeTrade(strategy, Trade.TradeType.BUY);
							logger.debug("Manually stopped, closing trade and exiting strategy");
							strategy.setStopped(new Date());
							strategyService.save(strategy);
						}
					} else {
						logger.debug("Confirmed long position for strategy: " + strategy);
						strategy.takeLongPosition();
						if(!doTrading) {
							makeTrade(strategy, Trade.TradeType.SELL);
							logger.debug("Manually stopped, closing trade and exiting strategy");
							strategy.setStopped(new Date());
							strategyService.save(strategy);
						}
					}
					strategy.setLastTradePrice(lastTrade.getPrice());
				} else if (strategy.hasLongPosition()) {
					logger.debug("Closing long position for strategy: " + strategy);
					logger.debug("Bought at: " + strategy.getLastTradePrice() + ", sold at: " + lastTrade.getPrice());
					closePosition(lastTrade.getPrice() - strategy.getLastTradePrice(), strategy);
					if(!doTrading) {
						logger.debug("Manually stopped, exiting strategy");
						strategy.setStopped(new Date());
						strategyService.save(strategy);
					}
				} else if (strategy.hasShortPosition()) {
					logger.debug("Closing short position for strategy: " + strategy);
					logger.debug("Sold at: " + strategy.getLastTradePrice() + ", bought at: " + lastTrade.getPrice());
					closePosition(strategy.getLastTradePrice() - lastTrade.getPrice(), strategy);
					if(!doTrading) {
						logger.debug("Manually stopped, exiting strategy");
						strategy.setStopped(new Date());
						strategyService.save(strategy);
					}
				}
			}

			lastTrade.setAccountedFor(true);
			tradeService.save(lastTrade);
	}
	
	public void resetTradeVariables() {
			
			shortHigher = 0;
			lastAveragePriceDifference = 0;
			exitProfit = 0;
			exitLoss = 0;
			
		}

}
