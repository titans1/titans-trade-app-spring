package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.service.SimpleStrategyService;


@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host}")
@RestController
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/titans/strategy}")
public class StrategyController {

	private static final Logger logger = LoggerFactory.getLogger(SimpleStrategy.class);

	@Autowired
	private SimpleStrategyService stratService;

	public StrategyController() {
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SimpleStrategy> findAll() {
		logger.debug("findAll()");
		return stratService.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public SimpleStrategy findById(@PathVariable int id) {
		logger.debug("findById(" + id + ")");
		return stratService.findById(id);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<SimpleStrategy> create(@RequestBody SimpleStrategy simpleStrat) {
		logger.debug("here");
		logger.debug("create(" + simpleStrat + ")");
		System.out.println("S"+simpleStrat.toString());
		simpleStrat.setId(stratService.save(simpleStrat));
		logger.debug("created trade: " + simpleStrat);

		return new ResponseEntity<SimpleStrategy>(simpleStrat, HttpStatus.CREATED);
	}

	//update strategy path
	
}
