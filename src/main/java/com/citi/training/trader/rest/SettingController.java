package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Setting;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.SettingService;
import com.citi.training.trader.service.SimpleStrategyService;

@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host}")
@RestController
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/titans/settings}")
public class SettingController {

	private static final Logger logger = LoggerFactory.getLogger(Setting.class);

	@Autowired
	private SettingService settingService;

	public SettingController() {
		// TODO Auto-generated constructor stub
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Setting> getAll() {
		logger.debug("get all setttings");
		return settingService.getAll();
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Setting> create(@RequestBody Setting setting) {

		setting.setId(settingService.save(setting));
		logger.debug("settting status"+ setting.getStatus());
		return new ResponseEntity<Setting>(setting, HttpStatus.CREATED);

	}
}
