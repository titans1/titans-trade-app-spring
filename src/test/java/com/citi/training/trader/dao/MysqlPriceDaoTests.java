package com.citi.training.trader.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.mysql.MysqlPriceDao;
import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlPriceDaoTests {
	
	@Autowired
	MysqlPriceDao mysqlPriceDao;
	
	@Autowired
	MysqlStockDao mysqlStockDao;
	
	
	@Test
    @Transactional
    public void test_create() {
		Stock testStock = new Stock(-1, "NTFL");
		int id = mysqlStockDao.create(testStock);
		//System.out.println("stock id"+testStock);
		int testPrice = mysqlPriceDao.create(new Price(mysqlStockDao.findById(id), 10.99));
		//System.out.println("test price"+testPrice);

		assertNotNull(testPrice);
    }
	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		
		Stock testStock = new Stock(-1, "NTFP");
		int id = mysqlStockDao.create(testStock);
		
		Price testPrice = new Price(mysqlStockDao.findById(id), 10.0);
		mysqlPriceDao.create(testPrice);
		
		int numberFound = mysqlPriceDao.findAll(testStock).size();
		assertTrue(numberFound > 0);
	}
	
	@Test
	@Transactional
	public void test_delete_older() {
		Stock testStock = new Stock(-1, "NTFP");
		int id = mysqlStockDao.create(testStock);
		Price testPrice = new Price(mysqlStockDao.findById(id), 10.0);
		mysqlPriceDao.create(testPrice);
		int numberDeleted = mysqlPriceDao.deleteOlderThan(new Date());
		assert(numberDeleted > 0);
	}
	
	

}
