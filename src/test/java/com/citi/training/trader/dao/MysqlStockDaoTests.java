//package com.citi.training.trader.dao;
//
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertNull;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.citi.training.trader.dao.mysql.MysqlStockDao;
//import com.citi.training.trader.exceptions.StockNotFoundException;
//import com.citi.training.trader.model.Price;
//import com.citi.training.trader.model.Stock;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@ActiveProfiles("h2")
//public class MysqlStockDaoTests {
//	
//	@Autowired
//	MysqlStockDao mysqlStockDao;
//	
//	@Test
//    @Transactional
//    public void test_createAndFindAll() {
//		Stock testStock = new Stock(10000, "AAPL");
//		
//		mysqlStockDao.create(testStock);
//		assertNotNull(mysqlStockDao.findAll());
//    }
//	
//	@Test
//    @Transactional
//    public void test_createAndFindByIdValid() {
//		Stock testStock = new Stock(-1, "AAPL");
//		
//		int id = mysqlStockDao.create(testStock);
//		assertNotNull(mysqlStockDao.findById(id));
//    }
//	
//	@Test(expected = StockNotFoundException.class)
//    @Transactional
//    public void test_createAndFindByIdInvalid() {
//		Stock testStock = new Stock(10000, "AAPL");
//		
//		mysqlStockDao.create(testStock);
//		assertNotNull(mysqlStockDao.findById(2));
//    }
//	
//	@Test
//    @Transactional
//    public void test_createAndFindByTickerValid() {
//		Stock testStock = new Stock(10000, "AAPL");
//		
//		mysqlStockDao.create(testStock);
//		assertNotNull(mysqlStockDao.findByTicker("AAPL"));
//    }
//	
//	@Test(expected = StockNotFoundException.class)
//    @Transactional
//    public void test_createAndFindByTickerInvalid() {
//		Stock testStock = new Stock(10000, "AAPL");
//		
//		mysqlStockDao.create(testStock);
//		assertNotNull(mysqlStockDao.findByTicker("AAAA"));
//    }
//	
//	@Test (expected = StockNotFoundException.class)
//    @Transactional
//    public void test_deleteByIdValid() {
//		Stock testStock = new Stock(10000, "AAPL");
//		
//		mysqlStockDao.create(testStock);
//		mysqlStockDao.deleteById(1);
//		
//		assertNull(mysqlStockDao.findById(1));
//    
//	}
//
//}
