//package com.citi.training.trader.dao;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//
//import java.util.Date;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.citi.training.trader.model.Price;
//import com.citi.training.trader.model.Stock;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@ActiveProfiles("h2")
//public class PriceDaoTests {
//	
//	@Autowired
//	PriceDao priceDao;
//	
//	@Autowired
//	StockDao stockDao;
//	
//	@Test
//	@Transactional
//	public void test_create_findAll(){
//		Stock testStock = new Stock(1, "AAPL");
//		stockDao.create(testStock);
//		Price testPrice = new Price(1, testStock, 10.99, new Date());
//		priceDao.create(testPrice);
//		
//		assertNotNull(priceDao.findAll(testStock));		
//	}
////	
////	@Test
////	@Transactional
////	public void test_findLatest() {
////		Stock testStock = new Stock(1, "AAPL");
////		stockDao.create(testStock);
////		Price testPrice = new Price(1, testStock, 10.99, new Date());
////		priceDao.create(testPrice);
////		
////		assertNotNull(priceDao.findLatest(testStock, 1));
////	}
//}
