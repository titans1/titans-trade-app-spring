//package com.citi.training.trader.dao;
//
//import static org.junit.Assert.assertArrayEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertNull;
//
//import java.util.List;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.citi.training.trader.dao.mysql.MysqlPriceDao;
//import com.citi.training.trader.dao.mysql.MysqlSimpleStrategyDao;
//import com.citi.training.trader.dao.mysql.MysqlStockDao;
//import com.citi.training.trader.dao.mysql.MysqlTradeDao;
//import com.citi.training.trader.exceptions.StockNotFoundException;
//import com.citi.training.trader.exceptions.TradeNotFoundException;
//import com.citi.training.trader.model.SimpleStrategy;
//import com.citi.training.trader.model.Stock;
//import com.citi.training.trader.model.Trade;
//import com.citi.training.trader.model.Trade.TradeState;
//import com.citi.training.trader.model.Trade.TradeType;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@ActiveProfiles("h2")
//public class MysqlTradeDaoTests {
//	
//	@Autowired
//	MysqlTradeDao mysqlTradeDao;
//	
//	@Autowired
//	MysqlPriceDao mysqlPriceDao;
//	
//	@Autowired
//	MysqlStockDao mysqlStockDao;
//	
//	@Autowired
//	MysqlSimpleStrategyDao mysqlSimpleStrategyDao;
//	
//	@Autowired
//	private JdbcTemplate tpl;
//
//	@Autowired
//	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
//	
//	private int testId = 1;
//    private double testPrice = 10.00;
//    private int testSize = 100;
//    //private SimpleStrategy testSimpleStrategy = new SimpleStrategy();
//	
//	@Test
//	@Transactional
//	public void test_save_FindAll() {
//		Stock testStock = new Stock(-1, "AAPL");
//		int testStockId = mysqlStockDao.create(testStock);
//		SimpleStrategy testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(testStockId), 100);
//		mysqlSimpleStrategyDao.save(testSimpleStrategy);
//		mysqlTradeDao.save(new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy));
//		List <Trade> listTrades = mysqlTradeDao.findAll();
//		assert(listTrades.size() >0);
//	}
//	
//	@Test
//	@Transactional
//	public void test_find_byId() {
//		
//		Stock testStock = new Stock(-1, "AAPL");
//		int testStockId = mysqlStockDao.create(testStock);
//		SimpleStrategy testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(testStockId), testSize);
//		mysqlSimpleStrategyDao.save(testSimpleStrategy);
//		int testTradeId = mysqlTradeDao.save(new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy));
//		Trade testTrade = mysqlTradeDao.findById(testTradeId);
//		
//		assert(testTrade.getId() == testTradeId);
//		assert(testTrade.getSize() == testSize);
//		assert(testTrade.getPrice() == testPrice);
//	}
//	
////	@Test
////	@Transactional
////	public void test_find_byStrategyId() {
////		Stock testStock = new Stock(-1, "AAPL");
////		int testStockId = mysqlStockDao.create(testStock);
////		SimpleStrategy testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(testStockId), 100);
////		int testStrategyId = mysqlSimpleStrategyDao.save(testSimpleStrategy);
////		int testTradeId = mysqlTradeDao.save(new Trade(testPrice, testSize, TradeType.BUY, mysqlSimpleStrategyDao.findAll().get(0)));
////		Trade testTrade = mysqlTradeDao.findLatestByStrategyId(testStrategyId);
////		
////		//assert(testTrade.getId() == testTradeId);
////		assert(testTrade.getPrice() == testPrice);
////	}
//	
//	@Test(expected = TradeNotFoundException.class)
//	@Transactional
//	public void test_delete_byIdValid() {
//		Stock testStock = new Stock(-1, "AAPL");
//		int testStockId = mysqlStockDao.create(testStock);
//		SimpleStrategy testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(testStockId), testSize);
//		mysqlSimpleStrategyDao.save(testSimpleStrategy);
//		int testTradeId = mysqlTradeDao.save(new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy));
//		//assertNotNull(testTradeId);
//		mysqlTradeDao.deleteById(testTradeId);
//		mysqlTradeDao.findById(testTradeId);
//	}
//	
//	@Test(expected = TradeNotFoundException.class)
//	@Transactional
//	public void test_delete_byIdInvalid() {
//		Stock testStock = new Stock(-1, "AAPL");
//		int testStockId = mysqlStockDao.create(testStock);
//		SimpleStrategy testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(testStockId), testSize);
//		mysqlSimpleStrategyDao.save(testSimpleStrategy);
//		int testTradeId = mysqlTradeDao.save(new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy));
//		//assertNotNull(testTradeId);
//		mysqlTradeDao.deleteById(testTradeId+10);
//		//mysqlTradeDao.findById(1);
//	}
//	
//	@Test
//	@Transactional
//	public void test_find_byState() {
//		Stock testStock = new Stock(-1, "AAPL");
//		int testStockId = mysqlStockDao.create(testStock);
//		SimpleStrategy testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(testStockId), 100);
//		mysqlSimpleStrategyDao.save(testSimpleStrategy);
//		mysqlTradeDao.save(new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy));
//		
//		List<Trade> listTrades = mysqlTradeDao.findAllByState(TradeState.INIT);
//		assert(listTrades.size() > 0);
//	}
//	
//	
//
//}
