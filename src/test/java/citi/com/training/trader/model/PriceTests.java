package citi.com.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

public class PriceTests {
	
	private int testId =1;
    private Stock testStock = new Stock(1, "AAPL");
    private double testPrice = 10.99;
    private Date testRecordedAt = java.sql.Date.valueOf("1999-01-01");
    
   
    
    @Test
    public void test_Price_constructor() {
    	
    	Price test_Price = new Price(testId, testStock, testPrice, testRecordedAt);
        assertEquals(testId, test_Price.getId());
        assertEquals(testStock, test_Price.getStock());
        assertEquals(testPrice, test_Price.getPrice(), 0.0001);
        assertEquals(testRecordedAt, test_Price.getRecordedAt());
    }
    
    @Test
    public void test_Price_toString() {
        String testString = new Price(testId, testStock, testPrice, testRecordedAt).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testStock.toString()));
        assertTrue(testString.contains(String.valueOf(testPrice)));
        assertTrue(testString.contains(testRecordedAt.toString()));
    }
    
    

}
