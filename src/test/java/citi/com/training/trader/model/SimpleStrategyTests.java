package citi.com.training.trader.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;

public class SimpleStrategyTests {
	
	private static final int DEFAULT_MAX_TRADES = 20;

    private int testId = 1;
    private Stock testStock = new Stock(-1, "GOOG");
    private int testSize =100;
    private double testExitProfitLoss =1000;
    private int testCurrentPosition =1;
    private double testLastTradePrice =100;
    private double testProfit = 100;
    private Date testStopped = new Date();
    private double testLongAverage = 99;
    private double testShortAverage = 95;
    
    @Test
    public void test_SS_Default_Constructor() {
    	SimpleStrategy testSimple = new SimpleStrategy();
    	
    	assertNotNull(testSimple);
    }
    
    @Test
    public void test_SS_Two_Parameter_Constructor() {
    	int testSize = 100;
    	
    	SimpleStrategy testSimple = new SimpleStrategy(testId, testStock, testSize, testExitProfitLoss, testCurrentPosition, testLastTradePrice, testProfit, testStopped);
    	assertNotNull(testSimple);
    }
    
    @Test
    public void test_SS_Eight_Parameter_Constructor() {
    	SimpleStrategy testSimple = new SimpleStrategy(testId, testStock, testSize, testExitProfitLoss, testCurrentPosition, testLastTradePrice, testProfit, testStopped);
    	assertTrue(testSimple.getSize() >0);
    }
    
    @Test
    public void test_Simple_StrategytoString() {
    	String testSimpleString = new SimpleStrategy(testId, testStock, testSize, testExitProfitLoss, testCurrentPosition, testLastTradePrice, testProfit, testStopped).toString();

    	  assertTrue(testSimpleString.contains((new Integer(testId)).toString()));
          assertTrue(testSimpleString.contains(testStock.toString()));
          assertTrue(testSimpleString.contains(String.valueOf(testSize)));
          assertTrue(testSimpleString.contains(String.valueOf(testExitProfitLoss)));
          assertTrue(testSimpleString.contains(String.valueOf(testCurrentPosition)));
          assertTrue(testSimpleString.contains(String.valueOf(testStopped)));
    }
    
    
    
    


}
