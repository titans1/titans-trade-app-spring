package citi.com.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.citi.training.trader.model.Stock;

public class StockTests {

	private int testId = 1;
    private String testTicker="AAPL";
    
    @Test
    public void test_Stock_constructor() {
    	
    	Stock test_Stock = new Stock(testId, testTicker);
        assertEquals(testId, test_Stock.getId());
        assertEquals(testTicker, test_Stock.getTicker());
  
    }
    
    @Test
    public void test_Stock_toString() {
        String testString = new Stock(testId, testTicker).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testTicker.toString()));

    }
}
