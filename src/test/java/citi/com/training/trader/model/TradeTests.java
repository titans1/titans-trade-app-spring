package citi.com.training.trader.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeType;

public class TradeTests {
	
	private int testId = 1;
    private double testPrice = 10.00;
    private int testSize = 100;
    private SimpleStrategy testSimpleStrategy = new SimpleStrategy();
    
//    private boolean accountedFor = ;
//    private Date LastStateChange = new Date();
//    this.setTradeType(tradeType);
    
    @Test
    public void test_default_constructor() {
    	Trade testTrade = new Trade();
    	//assertNotNull(testTrade);
    	assertTrue(testTrade.getSize() ==0);
    }
    
    @Test
    public void test_four_arg_constructor() {
    	Trade testTrade = new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy);
    	
    	assertEquals(testTrade.getPrice(), testPrice, 0.001);
    	assertEquals(testTrade.getSize(), testSize);
    	assertEquals(testTrade.getTradeType(), TradeType.BUY);
    	assertEquals(testTrade.getStrategy(), testSimpleStrategy);
    }
    
    @Test
    public void test_eight_arg_constructor() {
    	//Trade testTrade = new Trade()
    }
    
    @Test
    public void testSetTradeType() {
    	Trade testTrade = new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy);
    	testTrade.setTradeType(TradeType.SELL);
    	assert(testTrade.getTradeType() == TradeType.SELL);
    }
    
    @Test
    public void testSetTradeTypeXml() {
    	Trade testTrade = new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy);
    	
    	String testString = testTrade.getTradeTypeXml();
    	testTrade.setTradeTypeXml(testString);
    	assert(testTrade.getTradeTypeXml() != null);
    }
    
    @Test
    public void testGetTradeTypeXml() {
    	Trade testTrade = new Trade(testPrice, testSize, TradeType.BUY, testSimpleStrategy);
    	String testString = testTrade.getTradeTypeXml();
    	assertNotNull(testString);
    }

}
